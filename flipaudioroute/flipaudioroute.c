/*
** flipaudioroute.c -- flip audio route from remote to local and vice versa
** (C) Nick Garner, Pignology, LLC, 2014
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libconfig.h>
#include <getopt.h>

int debug = 1;
static int verbose_flag;

//settings:
config_t config;
config_setting_t *root, *setting;


int main(int argc, char **argv)
{

  printf("Flipping Audio Route.\n");


    int i;

    /* init libconfig */
    config_init(&config);
    if (! config_read_file(&config, "/opt/PigRemote/etc/pigremote.settings"))
    {
      fprintf(stderr, "%s:%d - %s\n", config_error_file(&config),
            config_error_line(&config), config_error_text(&config));
      config_destroy(&config);
      return(1);
    }

  config_lookup_int(&config, "pigremote.audio.route", &i);

  if (i == 1)
  {
    //it's 1, remote, set to 2 local
    printf("Old Audio Route: Remote (%d)\n", i);
    setting = config_lookup(&config, "pigremote.audio.route");                                         
    config_setting_set_int(setting, 2);
    printf("Audio Route flipped to Local (2)\n");
  }
  else
  {
    //it's 2, local, set to 1 remote 
    printf("Old Audio Route: Local (%d)\n", i);
    setting = config_lookup(&config, "pigremote.audio.route");                                         
    config_setting_set_int(setting, 1);
    printf("Audio Route flipped to Remote (1)\n");
  }


  config_write_file(&config, "/opt/PigRemote/etc/pigremote.settings");
    
  return 0;
}
