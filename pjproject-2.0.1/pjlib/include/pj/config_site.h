#define PJSIP_CHECK_VIA_SENT_BY   0


/*   
 * PJLIB settings.   
 */  
 
/* Disable floating point support */ 
#define PJ_HAS_FLOATING_POINT   0
 
/*   
 * PJMEDIA settings  
 */  
 
/* Disable some codecs */
#define PJMEDIA_HAS_L16_CODEC   0
#define PJMEDIA_HAS_G722_CODEC  0

/* Disable Echo Cancellation */
#define PJMEDIA_HAS_SPEEX_AEC    0
 
/* Use the built-in CoreAudio's iLBC codec (yay!) */ 
#define PJMEDIA_HAS_ILBC_CODEC  1
 
/* Fine tune Speex's default settings for best performance/quality */
#define PJMEDIA_CODEC_SPEEX_DEFAULT_QUALITY 5
 
/*   
 * PJSIP settings.   
 */  
 
/* Increase allowable packet size, just in case */   
//#define PJSIP_MAX_PKT_LEN 2000 
 
/*   
 * PJSUA settings.   
 */  
 
/* Default codec quality, previously was set to 5, however it is now 
 * set to 4 to make sure pjsua instantiates resampler with small filter. 
 */  
#define PJSUA_DEFAULT_CODEC_QUALITY 4
 
/* Set maximum number of dialog/transaction/calls to minimum */  
#define PJSIP_MAX_TSX_COUNT 31   
#define PJSIP_MAX_DIALOG_COUNT  31   
#define PJSUA_MAX_CALLS 1 
 
/* Other pjsua settings */   
#define PJSUA_MAX_ACC   1
#define PJSUA_MAX_PLAYERS   1
#define PJSUA_MAX_RECORDERS 1
#define PJSUA_MAX_BUDDIES   1  
