/*
** afraid_ddns_updater.c -- calls curl with the afraid update URL, called from cron every 4 hours
** (C) Nick Garner, Pignology, LLC, 2014
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
** 
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <libconfig.h>

int main(int argc, char *argv[])
{
    config_t config;
    config_init(&config);
    const char *afraid_url;

    config_init(&config);
    if (! config_read_file(&config, "/opt/PigRemote/etc/pigremote.settings"))
    {                                
      fprintf(stderr, "%s:%d - %s\n", config_error_file(&config),
            config_error_line(&config), config_error_text(&config));       
      config_destroy(&config);                                            
      exit(1);          
    } 
    /* Get afraid_url */                                                                      
    if (config_lookup_string(&config, "pigremote.afraid_ddns.afraid_url", &afraid_url))              
    {
      //
    }
    else
    {
      printf("Unable to find afraid_url setting, exiting.\n");
      exit(1);
    }

    if (strlen(afraid_url) < 5)
    {
      printf("afraid_url not defined, exiting.\n");
      exit(0);
    }

    char run[300];
    sprintf(run, "/usr/bin/curl %s", afraid_url);
    system(run);

    return 0;
}
