/* $Id: praudio-s.c 3553 2011-05-05 06:14:19Z nanang $ */
/* 
 * PigRemote Modifications (C) 2013 Pignology, LLC
 * Copyright (C) 2008-2011 Teluu Inc. (http://www.teluu.com)
 * Copyright (C) 2003-2008 Benny Prijono <benny@prijono.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

/**
 * praudio-s.c
 *
 * This is a very simple SIP User Agent based on the PJSUA library.
 * The application, when started, will initialize and then wait for a call
 * from a PRAudio client.  There is no SIP registration occuring,
 * audio from here to PRAudio on the remote device is point to point.
 * The application will purposefully exit after completing one call.
 * Systemd should be used to restart the application after termination.
 * 
 */

#include <pjsua-lib/pjsua.h>
#include <unistd.h>
#include <libconfig.h>

#define THIS_FILE	"PRAudio-S"

#define SIP_DOMAIN	"PigRemote"
#define SIP_USER	"PR"
#define SIP_PASSWD	"notsosecret"

#define SIP_PORT        37372            /* Listening SIP port           */
#define RTP_PORT        37373        /* RTP port                        */

int debug = 0;
int route = 0; // 1 = remote, 2 = local, get from settings
               // set to 0 and if settings fails, error out
const char *stunserver;

static pj_bool_t call_complete = 0;

/* Callback called by the library upon receiving incoming call */
static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id,
			     pjsip_rx_data *rdata)
{
    pjsua_call_info ci;

    PJ_UNUSED_ARG(acc_id);
    PJ_UNUSED_ARG(rdata);

    pjsua_call_get_info(call_id, &ci);

    PJ_LOG(3,(THIS_FILE, "Incoming call from %.*s!!",
			 (int)ci.remote_info.slen,
			 ci.remote_info.ptr));

    /* Automatically answer incoming calls with 200/OK */
    pjsua_call_answer(call_id, 200, NULL, NULL);
}

/* Callback called by the library when call's state has changed */
static void on_call_state(pjsua_call_id call_id, pjsip_event *e)
{
    pjsua_call_info ci;

    PJ_UNUSED_ARG(e);

    pjsua_call_get_info(call_id, &ci);
    PJ_LOG(3,(THIS_FILE, "Call %d state=(%d) %.*s", ci.state, call_id,
			 (int)ci.state_text.slen,
			 ci.state_text.ptr));
    if (ci.state == PJSIP_INV_STATE_DISCONNECTED)
    {
printf("Call is disconnected\n");
      call_complete = 1;
    }
}

/* Callback called by the library when call's media state has changed */
static void on_call_media_state(pjsua_call_id call_id)
{
    pjsua_call_info ci;

    pjsua_call_get_info(call_id, &ci);

    if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE) {
	// When media is active, connect call to sound device.
	pjsua_conf_connect(ci.conf_slot, 0);
	pjsua_conf_connect(0, ci.conf_slot);
    }
}

/* Display error and exit application */
static void error_exit(const char *title, pj_status_t status)
{
    pjsua_perror(THIS_FILE, title, status);
    pjsua_destroy();
    exit(1);
}

/*
 * main()
 *
 * argv[1] may contain URL to call.
 */
int main(int argc, char *argv[])
{

    //get settings from config
    config_t config;
    config_setting_t setting;
    config_init(&config);

    if (! config_read_file(&config, "/opt/PigRemote/etc/pigremote.settings"))
    {
      fprintf(stderr, "%s:%d - %s\n", config_error_file(&config),
            config_error_line(&config), config_error_text(&config));
      config_destroy(&config);
      return(1);
    }

    //check command line options
    int c;
    opterr = 0;
    while ((c = getopt (argc, argv, "d")) != -1)                      
    {                                                                 
      switch (c)                                                      
      {                                                               
        case 'd':
          debug = 1;
          break;
        case '?':
          if (optopt == 'c')
            fprintf (stderr, "Option -%c requires an argument.\n", optopt); //for future use
          else if (isprint (optopt))
            fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          else
            fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
            return 1;
        default:
            abort ();
      }
    } 

    if (debug)
    {
      printf("Debug is set.\n");
    }
    else
    {
      //fork replaced with systemd process management
    }

    /* get audio route */
    config_lookup_int(&config, "pigremote.audio.route", &route);                               
    if (route == 2)                                                                            
    {                                                                                          
      //if (debug) printf("Local audio routing enabled, disabling STUN\n");                      
      printf("Local audio routing enabled, disabling STUN\n");                      
      //PJ_LOG(3,(THIS_FILE, "Local audio routing enabled, disabling STUN"));
    }                                                                                          
    else if (route == 1)                                                                       
    {                                                                                          
      //if (debug) printf("Remote audio routing enabled, enabling STUN\n");                      
      printf("Remote audio routing enabled, enabling STUN\n");                      
    }                                                                                          
    else                                                                                       
    {                                                                                          
       fprintf(stderr, "Failed to get audio routing method from settings, assuming remote.\n");
       route = 1;                                                                              
    }

    /* Get STUN Server */
    if (config_lookup_string(&config, "pigremote.audio.stunserver", &stunserver))
    {                                                                            
      if (route == 1) printf("STUN Server: %s\n", stunserver);                   
    }                                                                            
    else                                                                         
    {                                                                            
      if (route == 1) stunserver = "stun1.pignology.net";                        
    } 

    pjsua_acc_id acc_id;
    pj_status_t status;

    /* Create pjsua first! */
    status = pjsua_create();
    if (status != PJ_SUCCESS) error_exit("Error in pjsua_create()", status);

    /* Init pjsua */
    {
	pjsua_config cfg;
	pjsua_logging_config log_cfg;
        pjsua_media_config      media_cfg;

	pjsua_config_default(&cfg);
	cfg.max_calls = 1;
	cfg.cb.on_incoming_call = &on_incoming_call;
	cfg.cb.on_call_media_state = &on_call_media_state;
	cfg.cb.on_call_state = &on_call_state;
        //STUN test
	if (route == 2)
	{
          cfg.stun_srv_cnt = 0;
	}
	else
	{
          cfg.stun_srv_cnt = 1;
          cfg.stun_srv[0] = pj_str((char*)stunserver);
	}

        pjsua_media_config_default(&media_cfg);
	media_cfg.no_vad = PJ_TRUE;
	//media_cfg.no_vad = PJ_FALSE;
	media_cfg.ec_tail_len = 0;
	//media_cfg.ec_tail_len = 10000;

	pjsua_logging_config_default(&log_cfg);
	if (debug) 
	{
	  log_cfg.console_level = 5;
	}
	else
	{
	  log_cfg.console_level = 5;
	}

	status = pjsua_init(&cfg, &log_cfg, &media_cfg);
	if (status != PJ_SUCCESS) error_exit("Error in pjsua_init()", status);
    }

    /* Add transport. */
    {
	pjsua_transport_config cfg;

	pjsua_transport_config_default(&cfg);
	cfg.port = SIP_PORT;
	status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &cfg, NULL);
	//status = pjsua_transport_create(PJSIP_TRANSPORT_TCP, &cfg, NULL);
	if (status != PJ_SUCCESS) error_exit("Error creating transport", status);
    }


    /* Initialization is done, now start pjsua */
    status = pjsua_start();
    if (status != PJ_SUCCESS) error_exit("Error starting pjsua", status);

    /* Register to SIP server by creating SIP account. */
    {
	pjsua_acc_config cfg;

	pjsua_acc_config_default(&cfg);
	cfg.id = pj_str("sip:" SIP_USER "@" SIP_DOMAIN);
	cfg.reg_uri = pj_str("sip:" SIP_DOMAIN);
	cfg.cred_count = 1;
	cfg.cred_info[0].realm = pj_str(SIP_DOMAIN);
	cfg.cred_info[0].scheme = pj_str("digest");
	cfg.cred_info[0].username = pj_str(SIP_USER);
	cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
	cfg.cred_info[0].data = pj_str(SIP_PASSWD);

	cfg.register_on_acc_add = PJ_FALSE;

        /* Codec Priority */
        /* Codec is chosen by the client, no need to set it here.
         * We don't intiate calls
        pj_str_t tmp;
        status = pjsua_codec_set_priority(pj_cstr(&tmp, "PCMU/8000"), 250);
        if (status != PJ_SUCCESS) {
          fprintf(stderr, "Error setting codec priority!\n");
          //printf("******** Error setting codec priority!\n");
        }
        */


	/* RTP Transport Config*/
        pjsua_transport_config  rtp_cfg;
        pjsua_transport_config_default(&rtp_cfg);
        rtp_cfg.port = RTP_PORT;
        cfg.rtp_cfg = rtp_cfg;

	status = pjsua_acc_add(&cfg, PJ_TRUE, &acc_id);
	if (status != PJ_SUCCESS) error_exit("Error adding account", status);
    }


    /* Wait until user press "q" to quit. */
    for (;;) {
      if (!debug)
      {
        sleep(1);
      }
      else
      {
	char option[10];

	puts("Press 'h' to hangup all calls, 'q' to quit");
	if (fgets(option, sizeof(option), stdin) == NULL) {
	    puts("EOF while reading stdin, will quit now..");
	    break;
	}

	if (option[0] == 'q')
	    break;

	if (option[0] == 'h')
	    pjsua_call_hangup_all();
      }

      if (call_complete)
      {
        break;
      }
    }

    /* Destroy pjsua */
    pjsua_destroy();

    return 0;
}
