/*
** srssc.c -- tcp <-> serial port bridge for Sierra Radio Systems Station Controller
** (C) Nick Garner, Pignology, LLC, 2014
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <termios.h>
#include <asm-generic/ioctls.h>
#include <time.h>
#include <libconfig.h>


#define PORT "7374"   // port we're listening on

int debug = 0;

//settings:
config_t config;

fd_set master;    // master file descriptor list                                                
fd_set read_fds;  // temp file descriptor list for select()                                     
int fdmax;        // maximum file descriptor number                                             

int serialfd;    // serial port file descriptor                                                 

int listener;     // listening socket descriptor                                                
int newfd;        // newly accept()ed socket descriptor                                         
struct sockaddr_storage remoteaddr; // client address                                           
socklen_t addrlen;                                                                              
char remoteIP[INET6_ADDRSTRLEN];                                                                
                                                                                                    
int yes=1;        // for setsockopt() SO_REUSEADDR, below                                       
int i, j, rv;                                                                                   
                                                                                                    
struct addrinfo hints, *ai, *p;
                                                                                                    
char buf[256];    // buffer for tcp client data                                                 
int nbytes;                                                                                     
                                                                                                    
char rfsp[256]; // buffer - received from serial port                                           
char rfspindex = 0;  
struct timeval charreceived;
struct timeval currtime;

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


void sendToConnectedClients()
{
  //if (debug) printf("sendToConnectedClients()\n");
  if (rfspindex == 0)
  {
    return;
  }

  //currtime = time((time_t*)0);
  int retval = gettimeofday(&currtime, 0);
  if (retval == -1) 
  {
    if (debug)
    {
      printf("Error getting time of day.\n"); 
    }
  }
  else
  { 
    if (debug) printf("got time of day: %d.%d\n", currtime.tv_sec, currtime.tv_usec); 
  }
  //printf("currtime: %d\n", currtime);

  //charreceived is set, if 0, when a char comes into the serial port.  we then weiat
  // 500ms and send the buffer, then reset charreceived.
  
  if (currtime.tv_sec - charreceived.tv_sec > 0 || currtime.tv_usec - charreceived.tv_usec > 500000)
  {
    if (debug) printf("it's been more than 500ms, send serial port buffer to connected clients.\n");
    for(j = 0; j <= fdmax; j++) {                                               
      // send to everyone!                                                    
      if (FD_ISSET(j, &master)) {                                             
        // except the listener and ourselves                                
        if (j != listener && j != i && j != serialfd) {                     
          if (send(j, rfsp, rfspindex, 0) == -1) {                            
            perror("send");                                             
          }                                                               
        }                                                                   
      }                                                                       
    }
    rfspindex = 0;
    //wipe rfsp
    int x;
    for (x = 0; x < 256; x++)
    {
      rfsp[x] = 0x00;
    }

    //reset charreceived timer
    charreceived.tv_sec = 0;                                    
    charreceived.tv_usec = 0;
  }
}

int main(int argc, char **argv)
{
    printf("Starting SRS SC Serial Proxy\n\n");

    /* init libconfig */
    config_init(&config);
    if (! config_read_file(&config, "/opt/PigRemote/etc/pigremote.settings"))
    {
      fprintf(stderr, "%s:%d - %s\n", config_error_file(&config),
            config_error_line(&config), config_error_text(&config));
      config_destroy(&config);
      return(1);
    }

    /* get command line options */
    int c;
     
    opterr = 0;
     
    /* 
     * change third argument for x: to require an argument to a switch
     */
    while ((c = getopt (argc, argv, "d")) != -1)
    {
      switch (c)
      {
        case 'd':
          debug = 1;
          break;
        case '?':
          if (optopt == 'c')
            fprintf (stderr, "Option -%c requires an argument.\n", optopt);
          else if (isprint (optopt))
            fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          else
            fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
            return 1;
        default:
            abort ();
      }
    }

    if (debug)
    {
      //printf("Debug enabled, not daemonizing.\n");
      printf("Debug enabled.\n");
    }
    else
    {
      //fork
      // don't fork, let systemd handle it
    }

    //initial set of charreceived timer                
    charreceived.tv_sec = 0;                                          
    charreceived.tv_usec = 0;

    /* open serial port */
    serialfd = open("/dev/ttyO4", O_RDWR | O_NOCTTY | O_NDELAY);
    if (serialfd == -1)
    {
      /*
      * Could not open the port.
      */

      perror("open_port: Unable to open /dev/ttyO5 - ");
      return;
    }
    else
    {
      fcntl(serialfd, F_SETFL, 0);
    }

    /* set serial port baud, etc. */
    struct termios serialoptions;

    /*
     * Get the current options for the port...
     */

     //don't get the current options, set what we want, 
     //zero out the termios struct and set only our options
    //tcgetattr(serialfd, &serialoptions);

    if (debug) printf("tcgetattr: pre serialoptions.c_cflag: %u\n", serialoptions.c_cflag);

    bzero(&serialoptions, sizeof(serialoptions));

    //set 8-N-1
    serialoptions.c_cflag &= ~PARENB;
    serialoptions.c_cflag &= ~CSTOPB;
    serialoptions.c_cflag &= ~CSIZE;                                       
    serialoptions.c_cflag |= CS8;    //8 bits                              
    serialoptions.c_cflag &= ~CRTSCTS; //no flow control 

    /*
     * Set the baud rates to settings baud...
     */
    int settingsbaud;
    if (config_lookup_int(&config, "pigremote.serial.srsscbaud", &settingsbaud))
    {
      if (debug) printf("Found baud in settings: %d\n", settingsbaud);
    }
    else
    {
      fprintf(stderr, "Couldn't find baud in settings file.\n");
    }
   
    switch (settingsbaud)
    {
      case 38400:
        cfsetispeed(&serialoptions, B38400);
        cfsetospeed(&serialoptions, B38400);
        break;
      case 19200:
        cfsetispeed(&serialoptions, B19200);
        cfsetospeed(&serialoptions, B19200);
        break;
      case 9600:
        cfsetispeed(&serialoptions, B9600);
        cfsetospeed(&serialoptions, B9600);
        break;
      case 4800:
        cfsetispeed(&serialoptions, B4800);
        cfsetospeed(&serialoptions, B4800);
        break;
      default:
        cfsetispeed(&serialoptions, B38400);
        cfsetospeed(&serialoptions, B38400);
        break;
    }

    //cfsetispeed(&serialoptions, B38400);
    //cfsetospeed(&serialoptions, B38400);

    /*
     * Enable the receiver and set local mode...
     */

    serialoptions.c_cflag |= (CLOCAL | CREAD);

    if (debug) printf("tcgetattr: post: serialoptions.c_cflag: %u\n", serialoptions.c_cflag);

    /*
     * Set the new options for the port...
     */

    tcsetattr(serialfd, TCSANOW, &serialoptions);


    //set up networking
    FD_ZERO(&master);    // clear the master and temp sets
    FD_ZERO(&read_fds);

    //add the serialfd to the fd_set
    FD_SET(serialfd, &master);

    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if ((rv = getaddrinfo(NULL, PORT, &hints, &ai)) != 0) {
        if (debug) fprintf(stderr, "srssc: %s\n", gai_strerror(rv));
        exit(1);
    }
    
    for(p = ai; p != NULL; p = p->ai_next) {
        listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (listener < 0) { 
            continue;
        }
        
        // lose the pesky "address already in use" error message
        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

        if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
            close(listener);
            continue;
        }

        break;
    }

    // if we got here, it means we didn't get bound
    if (p == NULL) {
        if (debug) fprintf(stderr, "srssc: failed to bind\n");
        exit(2);
    }

    freeaddrinfo(ai); // all done with this

    // listen
    if (listen(listener, 10) == -1) {
        perror("listen");
        exit(3);
    }

    // add the listener to the master set
    FD_SET(listener, &master);

    // keep track of the biggest file descriptor
    //fdmax = listener; // so far, it's this one
    fdmax = (listener > serialfd ? listener : serialfd) + 1;


    // main loop
    for(;;) {

         //select timeout, recreate each time
         struct timeval timeout;
         timeout.tv_sec = 0;
         timeout.tv_usec = 600000;
         //if (debug) printf("select timeout: %d.%d", timeout.tv_sec, timeout.tv_usec);

        read_fds = master; // copy it
        if (select(fdmax+1, &read_fds, NULL, NULL, &timeout) == -1) {
            perror("select");
            exit(4);
        }

        // run through the existing connections looking for data to read
        for(i = 0; i <= fdmax; i++) {
            if (FD_ISSET(i, &read_fds)) { // we got one!!
                if (i == listener) {
                    // handle new connections
                    addrlen = sizeof remoteaddr;
                    newfd = accept(listener,
                        (struct sockaddr *)&remoteaddr,
                        &addrlen);

                    if (newfd == -1) {
                        perror("accept");
                    } else {
                        FD_SET(newfd, &master); // add to master set
                        if (newfd > fdmax) {    // keep track of the max
                            fdmax = newfd;
                        }
                        if (debug) {
                          printf("srssc: new connection from %s on "
                            "socket %d\n",
                            inet_ntop(remoteaddr.ss_family,
                                get_in_addr((struct sockaddr*)&remoteaddr),
                                remoteIP, INET6_ADDRSTRLEN),
                            newfd);
                        }
                    }
                } 
                else if (i == serialfd) {
                  if (debug) printf("received data on serial port\n");
                                      
                  //if charreceived is 0.0, set it to now so we can send the buffer after 500ms
                  if (charreceived.tv_sec == 0 && charreceived.tv_usec == 0)
                  {
                    gettimeofday(&charreceived, 0);
                  }

                  fcntl(serialfd, F_SETFL, FNDELAY); //Return read immidiatly
                  //int bytes_available;
                  //ioctl(serialfd, FIONREAD, &bytes_available);
                  char rfsptempbuff[255];
                  int rfspnumbytes = read(serialfd, rfsptempbuff, 255);
                  rfsptempbuff[rfspnumbytes] = '\0';
                  if (debug) printf("read %d bytes from sp: %s\n", rfspnumbytes, rfsptempbuff);
                  //snprintf(&rfsp[rfspindex], rfspnumbytes, "%s", rfsptempbuff);
                  int x;
                  for (x = 0; x < rfspnumbytes; x++)
                  {
                    rfsp[rfspindex] = rfsptempbuff[x];
                    rfspindex++;
                  }
                  if (debug) printf("rfspindex is now %d: %s\n", rfspindex, rfsp);
                } 
                else {
                    // handle data from a client
                    if ((nbytes = recv(i, buf, sizeof buf, 0)) <= 0) {
                        // got error or connection closed by client
                        if (nbytes == 0) {
                            // connection closed
                            if (debug) printf("srssc: socket %d hung up\n", i);
                        } else {
                            perror("recv");
                        }
                        close(i); // bye!
                        FD_CLR(i, &master); // remove from master set
                    } else {
                        // we got some data from a client
                        //write to serial port
                        int n = write(serialfd, buf, nbytes);
                        if (n < 0)
                        {
                          if (debug) fputs("write() of 4 bytes failed!\n", stderr);
                        } else {
                          if (debug) printf("wrote %d to serial port\n", nbytes);
                        }

                        //send to everyone
                        /*for(j = 0; j <= fdmax; j++) {
                            // send to everyone!
                            if (FD_ISSET(j, &master)) {
                                // except the listener and ourselves
                                if (j != listener && j != i && j != serialfd) {
                                    if (send(j, buf, nbytes, 0) == -1) {
                                        perror("send");
                                    }
                                }
                            }
                        }*/
                    }
                } // END handle data from client
            } // END got new incoming connection
        } // END looping through file descriptors

      //send data from serial port if we have some more
      sendToConnectedClients();

    } // END for(;;)--and you thought it would never end!
    
    return 0;
}
